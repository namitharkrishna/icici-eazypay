$(document).ready(function() {
    // $(function() {
    //     $(".datepicker").datepicker();
    // });

    //........................................................//
    //.....al the functions for datepicker and dialog box.....//
    //........................................................//

    $(".datePickClass").datepicker({onSelect: function() { // Automatic close on selection
        // $('#dateInput').val($('#dpDlg').datepicker('getDate'));
        this.click();
        $("body").removeClass("overflow-hidden");
        // $('#dialog').dialog('close');
        },
            inline: true,
            showOtherMonths: true,
            dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
    });
    $(".mr-text-input-cont").click(function(){
        event.stopPropagation();
        if($(this).find(".mr-input").hasClass("datePickClass")){
            $(".mr-card-img-wrap").toggleClass("mr-date-blur-background");
            $(".mr-date-overlay").toggleClass("mr-hide");
            $("body").addClass("overflow-hidden");
        }
    });
    $(".mr-date-overlay").click(function(){
        $(".mr-card-img-wrap").toggleClass("mr-date-blur-background");
        $(".mr-date-overlay").toggleClass("mr-hide");
        $("body").removeClass("overflow-hidden");
    })

    // $('#dialog').dialog({autoOpen: false, modal: true,
    //     title: 'Select a date',
    //     buttons: {
    //         OK: function() {
    //             // $('#dateInput').val($('#dpDlg').datepicker('getDate'));
    //             $('#dialog').dialog('close');
    //             $(".ui-datepicker").hide();
    //         },
    //         Cancel: function() {
    //             $('#dialog').dialog('close');
    //         }
    //     }
    // });

    //............................................................//

    // $(".mr-text-input-cont").click(function() {
    //     $(this).find('.textfield-name').css('transform', "translate(0, 0px)");
    //     $(this).find(".mr-input").focus();
    //     $('#dialog').dialog('open');
    // })
    // $(".mr-text-input-cont input").focusout(function() {
    //     if ($(this).val().length == 0) {
    //         $(this).parent().find('.textfield-name').css('transform', "translate(0, 20px)");
    //     }
    // })
    $(".mr-burger-menu-icon").click(function(event) {
        event.stopPropagation();
        $(this).parents().find(".hamburger-main").toggleClass("active");
        $(this).parents().find(".hamburger-side").toggleClass("active");


    });
    $(".mr-hamburger-header").click(function(event) {
        event.stopPropagation();
    });
    $(".mr-hb-menu").click(function(event) {
        event.stopPropagation();
    });
    $('.hamburger-main').click(function() {
        $(".mr-burger-menu-icon").parents().find(".hamburger-main").removeClass("active");
        $(".mr-burger-menu-icon").parents().find(".hamburger-side").removeClass("active");

    })
    $('.oval-6').click(function() {
        $(this).closest(".mr-main-cont").find(".mr-grey-search").toggleClass("mr-hide");
        $(this).closest(".mr-search-block").find(".row").toggleClass("mr-special-set");
        $(this).toggleClass("mr-grey-color");
        $(this).find(".mr-filter").toggleClass("mr-hide");
        $(this).find(".mr-cross").toggleClass("mr-hide");
    })
    $(".mr-lang").click(function() {
        // $(this).find(".mr-tick").toggleClass("mr-hide");
        $(this).parent().find(".mr-tick").addClass("mr-hide");
        $(this).find(".mr-tick").removeClass("mr-hide");
    })

    //-----------------------------------------------------------------------------//
    //..............codes for animation of the generate bill page..................//
    //-----------------------------------------------------------------------------//

    $(".mr-white-receipt-small.first").click(function(){
        $(".mr-white-receipt-small.second").addClass("mr-receipt-move-left");
        $(".mr-white-receipt-small.third").addClass("mr-receipt-move-left");
        $(".mr-white-receipt-small.fourth").addClass("mr-receipt-move-left");
        $(this).find(".mr-arrow-op").addClass("down");
        var $this = $(this);
        setTimeout(function() {
            $this.find(".mr-white-rec-close-content").fadeOut();
            $this.find(".mr-white-rec-open").slideDown();
            // $(".mr-white-receipt-small.second").addClass("mr-hide");
            // $(".mr-white-receipt-small.third").addClass("mr-hide");
        }, 500);
        setTimeout(function() {
            $this.find(".mr-white-rec-open").removeClass("zero-opacity");
        }, 1000);
    })
    $(".mr-white-receipt-small.second").click(function(){
        $(".mr-white-receipt-small.first").addClass("mr-receipt-move-left");
        $(".mr-white-receipt-small.third").addClass("mr-receipt-move-left");
        $(".mr-white-receipt-small.fourth").addClass("mr-receipt-move-left");
        $(this).find(".mr-arrow-op").addClass("down");
        var $this = $(this);
        setTimeout(function() {
            $this.addClass("mr-second-move-up");
            $this.find(".mr-white-rec-close-content").fadeOut();
            $this.find(".mr-white-rec-open").slideDown();
        }, 500);
        setTimeout(function() {
            $this.find(".mr-white-rec-open").removeClass("zero-opacity");
        }, 1000);
    })
    $(".mr-white-receipt-small.third").click(function(){
        $(".mr-white-receipt-small.first").addClass("mr-receipt-move-left");
        $(".mr-white-receipt-small.second").addClass("mr-receipt-move-left");
        $(".mr-white-receipt-small.fourth").addClass("mr-receipt-move-left");
        $(this).find(".mr-arrow-op").addClass("down");
        var $this = $(this);
        setTimeout(function() {
            $this.addClass("mr-third-move-up");
            $this.find(".mr-white-rec-close-content").fadeOut();
            $this.find(".mr-white-rec-open").slideDown();
        }, 500);
        setTimeout(function() {
            $this.find(".mr-white-rec-open").removeClass("zero-opacity");
        }, 1000);
    })
    $(".mr-white-receipt-small.fourth").click(function(){
        $(".mr-white-receipt-small.first").addClass("mr-receipt-move-left");
        $(".mr-white-receipt-small.second").addClass("mr-receipt-move-left");
        $(".mr-white-receipt-small.third").addClass("mr-receipt-move-left");
        $(this).find(".mr-arrow-op").addClass("down");
        var $this = $(this);
        setTimeout(function() {
            $this.addClass("mr-fourth-move-up");
            $this.find(".mr-white-rec-close-content").fadeOut();
            $this.find(".mr-white-rec-open").slideDown();
        }, 500);
        setTimeout(function() {
            $this.find(".mr-white-rec-open").removeClass("zero-opacity");
        }, 1000);
    })
    $(".mr-arrow-op").click(function(){
        if($(this).hasClass("down")){
            event.stopPropagation();
            var $this = $(this).closest(".mr-white-receipt-small");
            $this.find(".mr-white-rec-open").addClass("zero-opacity");
            $this.find(".mr-white-rec-open").slideUp();
            $this.find(".mr-white-rec-close-content").fadeIn();
            setTimeout(function() {
                $(".mr-white-receipt-small.first").removeClass("mr-receipt-move-left");
                $(".mr-white-receipt-small.second").removeClass("mr-receipt-move-left");
                $(".mr-white-receipt-small.third").removeClass("mr-receipt-move-left");
                $(".mr-white-receipt-small.fourth").removeClass("mr-receipt-move-left");
                $this.find(".mr-arrow-op").removeClass("down");
            }, 500);
        }
    })
    $(".mr-arrow-op").click(function(){
        if($(this).hasClass("down")){
            event.stopPropagation();
            var $this = $(this).closest(".mr-white-receipt-small");
            $this.find(".mr-white-rec-open").addClass("zero-opacity");
            $this.find(".mr-white-rec-open").slideUp();
            $this.find(".mr-white-rec-close-content").fadeIn();
            $this.removeClass("mr-second-move-up");
            $this.removeClass("mr-third-move-up");
            $this.removeClass("mr-fourth-move-up");
            setTimeout(function() {
                $(".mr-white-receipt-small.first").removeClass("mr-receipt-move-left");
                $(".mr-white-receipt-small.second").removeClass("mr-receipt-move-left");
                $(".mr-white-receipt-small.third").removeClass("mr-receipt-move-left");
                $(".mr-white-receipt-small.fourth").removeClass("mr-receipt-move-left");
                $this.find(".mr-arrow-op").removeClass("down");
            }, 500);
        }
    })

    // $(".mr-white-receipt-small").click(function(){
    //     $(".mr-white-receipt-small").addClass("mr-receipt-move-left");
    //     $(this).removeClass("mr-receipt-move-left");
    //     var $this = $(this);
    //     setTimeout(function() {
    //         $(".mr-white-receipt-small").addClass("mr-hide");
    //         $this.removeClass("mr-hide");
    //         $this.find(".mr-white-rec-close-content").slideUp();
    //         $this.find(".mr-white-rec-open").slideDown();
    //     }, 800);
    // })



    // $(".mr-help-page .mr-white-receipt-small .mr-arrow").click(function() {
    //     $(this).parent().siblings().toggleClass("mr-hide");
    //     $(this).parent().toggleClass("mr-arrow-angle");


    // })
    // $(".mr-upi").click(function() {
    //     $(this).parent().find(".mr-white-receipt-small").addClass("mr-hide");
    //     $(this).parent().find(".Rectangle-17-Copy").removeClass("mr-hide");
    //     //$(this).parent().find(".mr-white-rec-open.open-upi").removeClass("mr-hide");
    //     $(this).parent().find('.mr-white-rec-open.open-upi').slideDown("slow", function() {
    //         // Animation complete.
    //     });


    // })
    // $(".mr-close-arrow").click(function() {
    //     $(this).parents().find(".mr-white-rec-open").slideUp("slow", function() {});

    //     setTimeout(function() {
    //         $(".mr-white-receipt-small").removeClass("mr-hide");

    //     }, 700);

    // })
    // $(".mr-cards-net").click(function() {
    //     $(this).parent().find(".mr-white-receipt-small").addClass("mr-hide");
    //     $(this).parent().find(".mr-white-rec-open.open-cards-net").slideDown("slow", function() {
    //         // Animation complete.
    //     });

    // })

    $(function() {
        $("#checkboxId input[type='checkbox']").change(function() {
            $(this).parents().find('.mr-select-adjust').toggleClass("mr-hide");
        })
    })
    $(function() {
        $("#checkboxId1 input[type='checkbox']").change(function() {
            $(this).parents().find('.mr-white-content').toggleClass("mr-hide");
            $(this).parents().find('.mr-white-content-fill').toggleClass("mr-hide");
            $(this).parents().find(".mr-code-text").toggleClass("orange");
            $(this).parents().find(".mr-upi-cnt-txt").toggleClass("grey");
        })
    })

    $(".cross").hide();
    $(".menu").hide();
    $(".hamburger").click(function() {
        $(".menu").slideToggle("slow", function() {
            $(".hamburger").hide();
            $(".cross").show();
        });
    });

    $(".cross").click(function() {
        $(".menu").slideToggle("slow", function() {
            $(".cross").hide();
            $(".hamburger").show();
        });
    });

    $(function() {
        var $write = $('#write');
        var password = [];

        $('#keyboard li').click(function() {
            if (password.length <= 6) {
                var $this = $(this),
                    character = $this.html(); // If it's a lowercase letter, nothing happens to this variable

                if ($this.hasClass('next')) {
                    return false;
                }
                if ($this.hasClass('delete')) {
                    var html = $write.html();
                    $(".mr-dot").removeClass("mr-white-dot");
                    for (i = 0; i <= password.length; i++) {
                        // $(".mr-dot").eq(i - 1).addClass("mr-white-dot");
                        $(".mr-dot").eq(i).removeClass("mr-white-dot");
                    }
                    password = [];
                    // $(".mr-dot").eq(i - 1).removeClass("mr-white-dot");
                    // $write.html(html.substr(0, html.length - 1));
                    return false;

                }

                // Add the character
                $write.html($write.html() + character);
                password.push(character);
                $(".mr-dot").removeClass("mr-white-dot");
                for (i = 0; i < password.length; i++) {
                    $(".mr-dot").eq(i).addClass("mr-white-dot");
                }
            }
            console.log(password);
        });
    });


    //function for the upi registration page

    $(".mr-upi-button").click(function() {
        var index = $(".mr-upi-button").index($(this));
        $(".mr-upi-button").removeClass("active");
        $(this).addClass("active");
        $(".mr-upi-description").addClass("mr-hide");
        $(".mr-upi-description").eq(index).removeClass("mr-hide");
    });


    //function for help page accordion animation

    $(".mr-help-sm .mr-white-receipt-small").on("click",function(){
        var parentClass = $(this).closest(".mr-help-sm");
        if($(this).find(".mr-help-content").attr("style") != "display: block;"){
            parentClass.find(".mr-white-receipt-small .mr-help-content").slideUp();
            parentClass.find(".mr-white-receipt-small").removeClass("mr-arrow-angle");
            $(this).find(".mr-help-content").slideDown();
            $(this).addClass("mr-arrow-angle");
        }
        else{
            $(this).find(".mr-help-content").slideUp();
            $(this).removeClass("mr-arrow-angle");
        }
    })



    //function for collection modes checkbox bhart qr code and aadhar pay

    $(".mr-model-check-boxes input").change(function(){
        var $this = $(this);
        if($(".mr-model-check-boxes input").eq(0)[0].checked == true || $(".mr-model-check-boxes input").eq(1)[0].checked == true){
            $this.closest(".mr-additional-modes-cont").find(".mr-model-check-results").removeClass("mr-hide");
        }
        else{
            $this.closest(".mr-additional-modes-cont").find(".mr-model-check-results").addClass("mr-hide");

        }
    })

    $("#onoffswitch").trigger("checked");
    

});
